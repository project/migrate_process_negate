<?php

namespace Drupal\migrate_process_negate\Plugin\migrate\process;

use Drupal\migrate\Row;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;

/**
 * Provides a 'Negate' migrate process plugin.
 *
 * It simply negates whatever variable is sent to it.
 *
 * @MigrateProcessPlugin(
 *  id = "negate"
 * )
 */
class Negate extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return !$value;
  }

}
