INTRODUCTION
------------

This is a simple migrate process plugin whose only purpose is to negate whatever value is sent over to it.

This is useful for situations where we want to save the opposite of what the source is.

For example, if our source content has a bool value of `is_deleted`, which roughly translates to Drupal's `status` (published) field, we actually want the content to be unpublished (0 or FALSE) when the source is marked as deleted (1 or TRUE).

Then all we'd need to do is:

```yml
process:
  status:
    plugin: negate
    source: is_deleted
```

And it would work as expected.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/migrate_process_negate

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/migrate_process_negate

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will provide a `negate` migrate process plugin.

MAINTAINERS
-----------

Current maintainers:
 * Miguel Guerreiro (gueguerreiro) - https://www.drupal.org/user/3589301

